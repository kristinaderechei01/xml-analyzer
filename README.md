# Smart XML Analyzer

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for testing purposes.

### Prerequisites

* Java 8
* Maven [3.3.9,)

### Installing

* Clone repository to your local machine using 'git clone https://kristinaderechei01@bitbucket.org/kristinaderechei01/xml-analyzer.git'
* Go to root folder of cloned project and open cmd
* Execute 'mvn clean compile assembly:single' command

## Running

To run the program navigate to 'target' folder and execute next command:
'java -jar xml-crawler-1.0-SNAPSHOT-jar-with-dependencies.jar <input_origin_file_path> <input_other_sample_file_path> <search_element_id>'

## Comparison results
Sample 0 vs Sample 1: html > body[1] > div > div[1] > div[2] > div > div > div[1] > a[1]  
Sample 0 vs Sample 2: html > body[1] > div > div[1] > div[2] > div > div > div[1] > div > a  
Sample 0 vs Sample 3: html > body[1] > div > div[1] > div[2] > div > div > div[2] > a  
Sample 0 vs Sample 4: html > body[1] > div > div[1] > div[2] > div > div > div[2] > a  

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [jsoup] (https://jsoup.org/) - XML reader