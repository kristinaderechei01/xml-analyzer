package com.derechei.web.analyzer.util.parser;

import com.derechei.web.analyzer.Demo;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class HtmlParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Demo.class);
    private static String CHARSET_NAME = "utf8";

    public static Optional<Document> parseHtml(File htmlFile) {
        try {
            return Optional.of(Jsoup.parse(
                    htmlFile,
                    CHARSET_NAME,
                    htmlFile.getAbsolutePath()));
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            return Optional.empty();
        }
    }
}
