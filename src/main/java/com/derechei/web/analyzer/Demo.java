package com.derechei.web.analyzer;

import com.derechei.web.analyzer.util.parser.HtmlParser;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;

import java.io.File;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

public class Demo {

    public static void main(String[] args) {
        String originFilePath = args[0];
        String diffPageFilePath = args[1];
        String targetElementId = args[2];

        assert (StringUtils.isNotEmpty(originFilePath));
        assert (StringUtils.isNotEmpty(diffPageFilePath));
        assert (StringUtils.isNotEmpty(targetElementId));

        Optional<Document> originDoc = HtmlParser.parseHtml(new File(originFilePath));
        Optional<Document> diffDoc = HtmlParser.parseHtml(new File(diffPageFilePath));
        Optional<Element> searchedElement = originDoc.map(document -> document.getElementById(targetElementId));

        if (searchedElement.isPresent() && diffDoc.isPresent()) {
            Optional<Element> targetElement = searchTargetElement(searchedElement.get(), diffDoc.get());
            Optional<String> path = processElementPath(targetElement);
            path.ifPresent(System.out::print);
        }
    }

    private static Optional<Element> searchTargetElement(Element searchElement, Document diffDoc) {
        StringBuilder searchElementSelector = new StringBuilder().append(searchElement.tagName());
        searchElement.classNames().forEach(className -> searchElementSelector.append(".").append(className));

        Elements parents = searchElement.parents();
        for (String className : searchElement.classNames()) {
            for (Element parent : parents) {
                Optional<Elements> possibleDiffParents = searchElement(parent, diffDoc);
                if (possibleDiffParents.isPresent()) {
                    Elements targetElementCandidates = Selector.select(searchElementSelector.toString(), possibleDiffParents.get());
                    if (targetElementCandidates.size() > 0) {
                        return Optional.of(targetElementCandidates.first());
                    }
                }
            }
            searchElementSelector.delete(searchElementSelector.lastIndexOf("."), searchElementSelector.length());
        }
        return Optional.empty();
    }

    private static Optional<String> processElementPath(Optional<Element> targetElement) {
        return targetElement.map(el -> {
            Elements parents = el.parents();
            parents.add(0, targetElement.get());
            Collections.reverse(parents);
            return parents.stream()
                    .map(element -> element.elementSiblingIndex() > 0
                            ? element.nodeName() + "[" + element.elementSiblingIndex() + "]"
                            : element.nodeName())
                    .collect(Collectors.joining(" > "));
        });
    }

    private static Optional<Elements> searchElement(Element searchFor, Element searchWhere) {
        StringBuilder selector = new StringBuilder().append(searchFor.tagName());
        searchFor.classNames().forEach(className -> selector.append(".").append(className));
        Elements elements = searchWhere.select(selector.toString());
        return elements.size() > 0 ? Optional.of(elements) : Optional.empty();
    }
}
